<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileUpdateRequest;
use App\Http\Requests\ProfileRequest;
use App\Models\Image;
use App\Models\User;
use Symfony\Component\HttpFoundation\Response;

class ProfileController extends Controller
{
    public function index(ProfileRequest $request)
    {
        $user = User::find($request->user_id)->with('image');
        return response([
            'status' => 'success',
            'data' => $user
        ], Response::HTTP_CREATED);
    }

    public function store(ProfileUpdateRequest $request)
    {
        $validatedData = $request->validated();

        // storage and create file
        $file = $request->file('image');
        if( $file ) {
            $image['path'] = 'storage/' . $file->store('images', 'public');
            $image['name'] = $file->hashName();
            $imageData = Image::create($image);
            $validatedData['image_id'] = $imageData->id;

            // get image path
            $validatedData['profilePath'] = $imageData->path;
        }
       
        // Update user
        if( !empty($validatedData['password']) ) {
            $validatedData['password'] = bcrypt($validatedData['password']);
        }
        
        $user = User::find($validatedData['id']);
        $user->update($validatedData);

        if( empty($validatedData['profilePath'])  ) {
            $imageData = Image::find( $user->image_id );
            $validatedData['profilePath'] = $imageData->path ?? null;
        }

        return response([
            'status' => 'success',
            'data' => $validatedData
        ], Response::HTTP_CREATED);
    }
    
    public function getProfiles()
    {
        $users = User::select('users.name','users.email','images.path as profilePath')
        ->leftJoin('images','images.id', '=', 'users.image_id' )
        ->get();
        return response([
            'status' => 'success',
            'data' => $users
        ], Response::HTTP_CREATED);
    }
}
