<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileUpdateRequest;
use App\Http\Requests\ProfileRequest;
use App\Models\Image;
use App\Models\User;
use Symfony\Component\HttpFoundation\Response;

class UsersController extends Controller
{
    public function getProfile(ProfileRequest $request)
    {
        $user = User::find();

        return response([
            'status' => 'success',
            'data' => $data
        ], Response::HTTP_CREATED);
    }
}
