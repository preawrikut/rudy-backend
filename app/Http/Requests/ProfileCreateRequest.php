<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProfileCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'min:3|max:50',
            'email' => [
                'required',
                'email',
                'unique:users'
            ],
            'password' => 'required|confirmed|min:6',
            // 'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            // 'id' => [
            //     'required',
            //     'integer',
            //     Rule::exists('users', 'id')
            // ]
        ];
    }
}
