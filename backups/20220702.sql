-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: default
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (1,'O18aCFu3RXh7KRFRNavGOzQWbMALpfWZTVEZBHl6.png','storage/images/O18aCFu3RXh7KRFRNavGOzQWbMALpfWZTVEZBHl6.png','2023-06-30 17:32:02','2023-06-30 17:32:02'),(2,'iqpkSGV664lYYEzVjZrgY4DLVkrjvHjwrI6LCM26.png','storage/images/iqpkSGV664lYYEzVjZrgY4DLVkrjvHjwrI6LCM26.png','2023-06-30 17:33:02','2023-06-30 17:33:02'),(3,'WaVcYhHF6TJLJxP0Lvbphk3U5xVNT0f3wXTO7Mo6.png','storage/images/WaVcYhHF6TJLJxP0Lvbphk3U5xVNT0f3wXTO7Mo6.png','2023-06-30 17:34:55','2023-06-30 17:34:55'),(4,'tKc9Ai3r2fCVQlg6gUjqbnIWXBYVpRfHfR0ytApp.png','storage/images/tKc9Ai3r2fCVQlg6gUjqbnIWXBYVpRfHfR0ytApp.png','2023-06-30 17:37:26','2023-06-30 17:37:26'),(5,'NfD0BNHSWz2KcI87sODSm2ViyRDPRp0Mjasq4B1Q.png','storage/images/NfD0BNHSWz2KcI87sODSm2ViyRDPRp0Mjasq4B1Q.png','2023-06-30 17:39:35','2023-06-30 17:39:35'),(6,'8m82f4n3IBgatvcCv5UuNx73uyzWLeB6klqKjdPF.png','storage/images/8m82f4n3IBgatvcCv5UuNx73uyzWLeB6klqKjdPF.png','2023-06-30 18:15:08','2023-06-30 18:15:08'),(7,'VgYKLDzTTUU8RtQQcTeUbwF48P0uHbb7Fq9vQPuW.png','storage/images/VgYKLDzTTUU8RtQQcTeUbwF48P0uHbb7Fq9vQPuW.png','2023-06-30 18:17:47','2023-06-30 18:17:47'),(8,'W2MMVndM6QeQq3DKkIf5JjPIijQOJZPDvz3Xte30.png','storage/images/W2MMVndM6QeQq3DKkIf5JjPIijQOJZPDvz3Xte30.png','2023-06-30 18:18:12','2023-06-30 18:18:12'),(9,'0rQc9WbH7mlzFrAhlV9AmqYgBeufscEbatQMCjF9.png','storage/images/0rQc9WbH7mlzFrAhlV9AmqYgBeufscEbatQMCjF9.png','2023-06-30 20:24:09','2023-06-30 20:24:09'),(10,'MAuld5DFUqoqiKKLIoHduS9zb3LthaSceL9wbugD.png','storage/images/MAuld5DFUqoqiKKLIoHduS9zb3LthaSceL9wbugD.png','2023-07-01 08:24:50','2023-07-01 08:24:50'),(11,'1P2JKTIjivaQRyC0tadfJPkBkuwkNcXP4nIYVOfa.png','storage/images/1P2JKTIjivaQRyC0tadfJPkBkuwkNcXP4nIYVOfa.png','2023-07-01 08:27:45','2023-07-01 08:27:45'),(12,'U9m20U0WGSDnGPTXDOAWe8Vi191xuguB7Y9oG3xx.png','storage/images/U9m20U0WGSDnGPTXDOAWe8Vi191xuguB7Y9oG3xx.png','2023-07-01 08:28:24','2023-07-01 08:28:24'),(13,'qGCihRHjvr8NxjXiOGT3odT6PCJoV8MbTmqYgOI6.png','storage/images/qGCihRHjvr8NxjXiOGT3odT6PCJoV8MbTmqYgOI6.png','2023-07-01 09:12:24','2023-07-01 09:12:24'),(14,'zygjzhJQn1EQnivP40o1M0Sg1PWcJ2wfPOjHWthk.png','storage/images/zygjzhJQn1EQnivP40o1M0Sg1PWcJ2wfPOjHWthk.png','2023-07-01 09:20:32','2023-07-01 09:20:32'),(15,'ohAh06mCBYGRtyg0G4z7iuFAJ8N8RDeyxO5bhhM1.png','storage/images/ohAh06mCBYGRtyg0G4z7iuFAJ8N8RDeyxO5bhhM1.png','2023-07-01 14:50:48','2023-07-01 14:50:48'),(16,'48HA4EBjpHlCt2yYOgtO8G0lUk7oEEvB1hwlOV2q.png','storage/images/48HA4EBjpHlCt2yYOgtO8G0lUk7oEEvB1hwlOV2q.png','2023-07-01 14:51:09','2023-07-01 14:51:09'),(17,'HepN7axYt9I2Vbi1YaithKEdMEi6ysE31OhM5WXt.png','storage/images/HepN7axYt9I2Vbi1YaithKEdMEi6ysE31OhM5WXt.png','2023-07-01 14:51:22','2023-07-01 14:51:22'),(18,'DP7ZlM5EvuMTgKtgJmFHpFCWlX5L6aJeosIs8gMG.png','storage/images/DP7ZlM5EvuMTgKtgJmFHpFCWlX5L6aJeosIs8gMG.png','2023-07-01 14:52:27','2023-07-01 14:52:27'),(19,'lHrpr1db4mPYzmMy98fltiXbN2V2XM2osPZjwVJ3.jpg','storage/images/lHrpr1db4mPYzmMy98fltiXbN2V2XM2osPZjwVJ3.jpg','2023-07-01 14:56:26','2023-07-01 14:56:26'),(20,'kEhCggCrBZavyzZXr4wQPaeCcV2OXx0tHjeEsgRz.jpg','storage/images/kEhCggCrBZavyzZXr4wQPaeCcV2OXx0tHjeEsgRz.jpg','2023-07-01 15:04:19','2023-07-01 15:04:19'),(21,'e1wlQhD1yVFN4wKvuzp6o3VSRhbqxpLGBscF6B3A.jpg','storage/images/e1wlQhD1yVFN4wKvuzp6o3VSRhbqxpLGBscF6B3A.jpg','2023-07-02 09:42:04','2023-07-02 09:42:04'),(22,'BnhGkD2SoMgfqAEV42nwfZDYXpEuCHK3dFsi7sxH.jpg','storage/images/BnhGkD2SoMgfqAEV42nwfZDYXpEuCHK3dFsi7sxH.jpg','2023-07-02 09:49:26','2023-07-02 09:49:26'),(23,'LJK3bklaTBUJU4tWeV1uc4kjjmWE8OYQkXMuHrvI.jpg','storage/images/LJK3bklaTBUJU4tWeV1uc4kjjmWE8OYQkXMuHrvI.jpg','2023-07-02 09:52:25','2023-07-02 09:52:25'),(24,'7t3OtP8BZZrMfD1EL5fX0uWz23uacZNQxPb6Q1LY.jpg','storage/images/7t3OtP8BZZrMfD1EL5fX0uWz23uacZNQxPb6Q1LY.jpg','2023-07-02 09:52:47','2023-07-02 09:52:47'),(25,'GL0H1vM1PH0bSG0mOwSZc4JoLmdturRSUcOlJv56.jpg','storage/images/GL0H1vM1PH0bSG0mOwSZc4JoLmdturRSUcOlJv56.jpg','2023-07-02 11:41:41','2023-07-02 11:41:41'),(26,'jzI98vWXQjFCCLyouyLgpYR537YwrnZxucB8xD6w.jpg','storage/images/jzI98vWXQjFCCLyouyLgpYR537YwrnZxucB8xD6w.jpg','2023-07-02 12:09:35','2023-07-02 12:09:35'),(27,'O5dMrSBm9kCbD2fR6w659pnHvUPFYHOnfYVhj8fY.jpg','storage/images/O5dMrSBm9kCbD2fR6w659pnHvUPFYHOnfYVhj8fY.jpg','2023-07-02 12:12:12','2023-07-02 12:12:12'),(28,'hqd8JQoOODFFVhxsaNkBQteGh9elfvRNvOVL6Kt9.jpg','storage/images/hqd8JQoOODFFVhxsaNkBQteGh9elfvRNvOVL6Kt9.jpg','2023-07-02 12:15:27','2023-07-02 12:15:27'),(29,'2kItFoaIsVYkHFSLgQftRhubXc6Hl7JqEV6JebUt.jpg','storage/images/2kItFoaIsVYkHFSLgQftRhubXc6Hl7JqEV6JebUt.jpg','2023-07-02 13:05:24','2023-07-02 13:05:24'),(30,'FlqFZlun5Ep7iG7UZCuV953FkQtMG4PWJGk4FpeU.jpg','storage/images/FlqFZlun5Ep7iG7UZCuV953FkQtMG4PWJGk4FpeU.jpg','2023-07-02 13:06:02','2023-07-02 13:06:02'),(31,'KyRFImRFYfIKjdUjtf5r8Qr0uiZGbC3xzvJpBvyx.jpg','storage/images/KyRFImRFYfIKjdUjtf5r8Qr0uiZGbC3xzvJpBvyx.jpg','2023-07-02 13:07:25','2023-07-02 13:07:25'),(32,'cEnwEIrZO2InzCXAnr7Rc9dzvh8vmKPlJbjvWHVw.jpg','storage/images/cEnwEIrZO2InzCXAnr7Rc9dzvh8vmKPlJbjvWHVw.jpg','2023-07-02 13:13:32','2023-07-02 13:13:32'),(33,'EkAIlCPIieUOz8coYPTIzkxMPUOkgP1wTdIhMn8I.jpg','storage/images/EkAIlCPIieUOz8coYPTIzkxMPUOkgP1wTdIhMn8I.jpg','2023-07-02 13:14:39','2023-07-02 13:14:39');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (6,'2014_10_12_000000_create_users_table',1),(7,'2014_10_12_100000_create_password_resets_table',1),(8,'2019_08_19_000000_create_failed_jobs_table',1),(9,'2019_12_14_000001_create_personal_access_tokens_table',1),(10,'2023_06_29_145440_create_images_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
INSERT INTO `personal_access_tokens` VALUES (1,'App\\Models\\User',1,'authToken','5b8aaf27ca333c4e50c606d1a2ff02fc028f6f1dddde357f4ec9312b379e2f5b','[\"*\"]','2023-07-02 11:12:57','2023-06-30 17:44:42','2023-07-02 11:12:57'),(2,'App\\Models\\User',1,'authToken','8739e36da4f23218b7d01396722b39a7cbba7a9820ebcd178e679fcb3f65d0c9','[\"*\"]',NULL,'2023-06-30 18:41:34','2023-06-30 18:41:34'),(3,'App\\Models\\User',1,'authToken','ecb3c024acf002dd6d3a4604e64103be08e5761bfa2d29d4e03d61c7194d310d','[\"*\"]',NULL,'2023-06-30 18:42:31','2023-06-30 18:42:31'),(4,'App\\Models\\User',1,'authToken','de8bbb2da0ae703c450162e58d01054ad30a5b75e90ce5463ebbdc9b8b013d5b','[\"*\"]',NULL,'2023-06-30 18:42:33','2023-06-30 18:42:33'),(5,'App\\Models\\User',1,'authToken','f9f3b5d798101af3d602a185392cbc617e55d455dd475a507c7da3ff7994e4e0','[\"*\"]',NULL,'2023-06-30 18:42:38','2023-06-30 18:42:38'),(6,'App\\Models\\User',1,'authToken','012832d8e29b5095f986069a007b0eb9fdd955e5e98a13c657930a1a84d6ee1a','[\"*\"]',NULL,'2023-07-01 03:11:50','2023-07-01 03:11:50'),(7,'App\\Models\\User',1,'authToken','dfe4d272ac2457cee9055a937d02456726ec61d74569b57a7479c09089ecc115','[\"*\"]',NULL,'2023-07-01 03:49:10','2023-07-01 03:49:10'),(8,'App\\Models\\User',1,'authToken','2a740fb0f1ecbf71febbfda0cdfdf3fbe2cc04f75378a6b88062bd51d95de2cc','[\"*\"]',NULL,'2023-07-01 03:52:57','2023-07-01 03:52:57'),(9,'App\\Models\\User',1,'authToken','0de599143637c61d36b6115c99b85086597c696740bf53d2fa5ee72a3dc913e2','[\"*\"]',NULL,'2023-07-01 03:53:12','2023-07-01 03:53:12'),(10,'App\\Models\\User',1,'authToken','bb738129c13e27441fc90f0adf0bb7020960f09f249467808872fcadc95cdc6b','[\"*\"]',NULL,'2023-07-01 03:54:03','2023-07-01 03:54:03'),(11,'App\\Models\\User',1,'authToken','946be1fd4d5c07230acb4a6af646606b15da06fdb835e6fc617590dff7accadd','[\"*\"]',NULL,'2023-07-01 03:54:38','2023-07-01 03:54:38'),(12,'App\\Models\\User',1,'authToken','6cc6bdbe1a9cf8bc856ea37b0218282f95e27d5dd653dd28c6a556d3491804c6','[\"*\"]',NULL,'2023-07-01 04:10:12','2023-07-01 04:10:12'),(13,'App\\Models\\User',1,'authToken','e97b6ce5a07343512ec8f0efc42736cee186b17fdca144fc3976cf21b3bd5f6f','[\"*\"]',NULL,'2023-07-01 04:20:09','2023-07-01 04:20:09'),(14,'App\\Models\\User',1,'authToken','a4ad6ff25dccbf67ba4ac2754f94327f8b64ec9bd25be65349380b5e75b0d62e','[\"*\"]',NULL,'2023-07-01 04:31:06','2023-07-01 04:31:06'),(15,'App\\Models\\User',1,'authToken','ec91638e13dfaaaea63b1da84aee4232037346fc8e33f97b257a2862b8a3ed39','[\"*\"]',NULL,'2023-07-01 04:33:23','2023-07-01 04:33:23'),(16,'App\\Models\\User',1,'authToken','6cf35ba7b9c9b8c96c3c30707015588f25d1e9689e67ae7cfdb87aba86b46635','[\"*\"]',NULL,'2023-07-01 04:53:50','2023-07-01 04:53:50'),(17,'App\\Models\\User',1,'authToken','263e39b43c78c21762a697a699823616ca2e34fd13ceee539a51cbee8e84995c','[\"*\"]',NULL,'2023-07-01 04:59:16','2023-07-01 04:59:16'),(18,'App\\Models\\User',1,'authToken','cf516939e4fdab74db9da2dd1f4474c6b94bc1131f1c19ff9d1f1968c12e2033','[\"*\"]',NULL,'2023-07-01 05:10:37','2023-07-01 05:10:37'),(19,'App\\Models\\User',1,'authToken','26a0a645f9ae639fb46e300251902a9ef7ac00bfa275550107ef294105a1c488','[\"*\"]',NULL,'2023-07-01 05:26:47','2023-07-01 05:26:47'),(20,'App\\Models\\User',1,'authToken','ae777c8131f126df399ad4a3541ae480eebf315ffac9b4e97e495bf894f03b3a','[\"*\"]',NULL,'2023-07-01 05:29:04','2023-07-01 05:29:04'),(21,'App\\Models\\User',1,'authToken','55e0c8aa468210de3ef81659d48da75b65d21c8bf20d5d906889c5a5f55d0a21','[\"*\"]',NULL,'2023-07-01 08:15:32','2023-07-01 08:15:32'),(22,'App\\Models\\User',1,'authToken','323a5a33dce53678761396ec364db13afecec6709d4680538b6f384e8007af64','[\"*\"]',NULL,'2023-07-01 08:16:20','2023-07-01 08:16:20'),(23,'App\\Models\\User',1,'authToken','5cff3479afa0ef40c9ed305dba0ae954fc11454a2963207098b2847366ce7daf','[\"*\"]',NULL,'2023-07-01 08:16:23','2023-07-01 08:16:23'),(24,'App\\Models\\User',1,'authToken','8b631ba7b6b1bdcf46da18221bac8a3c05e231612a093abf1d073761ad5e2076','[\"*\"]',NULL,'2023-07-01 08:17:12','2023-07-01 08:17:12'),(25,'App\\Models\\User',1,'authToken','fca5ee08468ed6ff174ef0d4bea8325eb56b9ef3a7cf903c6211dab6384c7eef','[\"*\"]','2023-07-01 09:16:21','2023-07-01 08:45:03','2023-07-01 09:16:21'),(26,'App\\Models\\User',1,'authToken','4d6d6643db8e91b18b782d20e0bc8a490971a3844f3e53416e2c03d3129be420','[\"*\"]','2023-07-01 09:21:19','2023-07-01 09:18:16','2023-07-01 09:21:19'),(27,'App\\Models\\User',1,'authToken','9103b81ba986df2629d4237c4d0f94808e63b45b5888424ec13d3ae8e5253ef5','[\"*\"]',NULL,'2023-07-01 14:45:47','2023-07-01 14:45:47'),(28,'App\\Models\\User',1,'authToken','e44e5c6253c457bbe14c2ce1d0ebd1b15056c4066a21ae64b02922533f834736','[\"*\"]',NULL,'2023-07-01 14:46:27','2023-07-01 14:46:27'),(29,'App\\Models\\User',1,'authToken','c966547c451e4654476ba905b0d6841c0c8d539001475c4b1ff446ad51b96d79','[\"*\"]','2023-07-01 15:04:19','2023-07-01 14:55:44','2023-07-01 15:04:19'),(30,'App\\Models\\User',1,'authToken','4122fa28d8657ac34fcb8997e854b383a9fe568bfd17b1bbe1e12b94e2288248','[\"*\"]',NULL,'2023-07-01 15:34:35','2023-07-01 15:34:35'),(31,'App\\Models\\User',1,'authToken','ef90c7bd170afeb5d7893ebaa89772e37d78b6800c2898ac2fefd45eb6c0657d','[\"*\"]',NULL,'2023-07-01 15:39:07','2023-07-01 15:39:07'),(32,'App\\Models\\User',1,'authToken','d9763063c05dbbfc12c1273ff5b9ca9bee09973da0133f9e004cf6adf669ce0a','[\"*\"]',NULL,'2023-07-01 16:13:39','2023-07-01 16:13:39'),(33,'App\\Models\\User',1,'authToken','a4029cec0cf78fb959c9d30014fb74548683e11a7eccc673ae5cb40fdd62101d','[\"*\"]',NULL,'2023-07-01 16:16:50','2023-07-01 16:16:50'),(34,'App\\Models\\User',12,'authToken','8d5527820ea4baa7ceecdcc515adc84d1711d0e4db99a5ff129dbf822333f814','[\"*\"]',NULL,'2023-07-02 08:49:47','2023-07-02 08:49:47'),(35,'App\\Models\\User',12,'authToken','e5edb49593f1c3b79de3ce730b4f2753a44e966d17f4ab834908e89176208d2a','[\"*\"]',NULL,'2023-07-02 08:50:20','2023-07-02 08:50:20'),(36,'App\\Models\\User',12,'authToken','e6f774a5849f4f3615669f210ba7309c106328636bf16f41574e83467b9132a9','[\"*\"]',NULL,'2023-07-02 09:10:00','2023-07-02 09:10:00'),(37,'App\\Models\\User',12,'authToken','6b4457f012106f30f70f376d68694087eb3ec6beab26718069fc55c280d8c6e8','[\"*\"]',NULL,'2023-07-02 09:10:03','2023-07-02 09:10:03'),(38,'App\\Models\\User',12,'authToken','1cb16ef13181585c346d5dd617a8855077da4f378c4faa01ce1771b3bb2e6393','[\"*\"]',NULL,'2023-07-02 09:18:08','2023-07-02 09:18:08'),(39,'App\\Models\\User',12,'authToken','5dd99cc662730251a2e14e5ea12467f265de10df367800a5cfedf1a11a9f0694','[\"*\"]','2023-07-02 09:52:47','2023-07-02 09:18:59','2023-07-02 09:52:47'),(40,'App\\Models\\User',1,'authToken','e4557d7c9a36b45486f5650c6929a1fc58d921b852560a84beb86cb61239d0fe','[\"*\"]','2023-07-02 11:08:49','2023-07-02 11:00:25','2023-07-02 11:08:49'),(41,'App\\Models\\User',1,'authToken','67b6dc2e5b77fb23c0ae846e4eec6d9f060caae9d779bc2b480dd33022323dac','[\"*\"]','2023-07-02 12:16:37','2023-07-02 11:09:20','2023-07-02 12:16:37'),(42,'App\\Models\\User',1,'authToken','78035d2c6eef7afa76a569471a9c78fd0ee551773d5ba336cf3d07175104e3fa','[\"*\"]','2023-07-02 12:19:27','2023-07-02 12:17:26','2023-07-02 12:19:27'),(43,'App\\Models\\User',1,'authToken','8a968e46569d3a7c1ad1818af1b6d3afed742d216fdae10b58c79535a9950338','[\"*\"]','2023-07-02 13:19:37','2023-07-02 12:19:46','2023-07-02 13:19:37');
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_id` bigint(20) unsigned DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'kittiwat','preawrikut@gmail.com',NULL,'$2y$10$scbOCEGQ4jsDZf3tPsfb1udJuDS8EaA0hTy098sxFfdVM/ZooOb8O',33,NULL,'2023-06-30 16:43:31','2023-07-02 13:14:39'),(2,'test','test@mail.com',NULL,'$2y$10$f6DPnKcKv0UNGi8wxcnlJO9uM7BkMF9KWbyfOoIufqE5tJr2c2tOe',NULL,NULL,'2023-07-02 08:29:26','2023-07-02 08:29:26'),(12,'test2','test2@mail.com',NULL,'$2y$10$gBE0xMxNr3OhimG1yrR4pOyFuvpDasvshFk0PcEJbEcaWU5CxPsJC',24,NULL,'2023-07-02 08:48:00','2023-07-02 09:52:47');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-02 13:37:41
